package nl.utwente.di.bookQuote;

import java.util.HashMap;

import static javax.swing.UIManager.put;

public class Quoter {

    HashMap<String, Double> quoter = new HashMap<>(){{
        put("1", 10.0);
        put("2", 45.0);
        put("3", 20.0);
        put("4", 35.0);
        put("5", 50.0);
    }};

    double getBookPrice(String isbn){
        if(quoter.get(isbn) != null){
            return quoter.get(isbn);
        }
        return 0;
    }
}
